.. WARNING::
    All of this documentation is obsolete!  Mailman Bundler is no longer
    recommended or supported.  Do not use it to try to install GNU Mailman 3,
    you will be frustrated.  Please go `see the Mailman Suite documentation
    <http://docs.mailman3.org/en/latest/>`_ instead.  It includes links for
    setting up all the components.
